from .app import app
from flask import render_template
from .models import get_sample, get_book_detail


@app.route("/")
def home():
    return render_template(
        "home.html",
        title="Tiny Amazon",
        books=get_sample())


@app.route("/detail/<bookid>")
def detail(bookid):
    return render_template(
        "detail.html",
        title="Détail",
        book=get_book_detail(int(bookid))
    )
